import numpy as np

def una_izquierda_sin_suma(matriz):
    es_cero = matriz[:,:-1] == 0
    ceros = np.where(es_cero)
    matriz[ceros] = matriz[ceros[0],ceros[1] + 1]

    matriz[ceros[0],ceros[1] + 1] = 0 
    return matriz

def izquierda(matriz):
    matriz_vieja = np.zeros(matriz.shape)
    rep = 0
    while np.sum(matriz == matriz_vieja) == np.sum(matriz.shape) and rep != len(matriz) - 1:
        matriz_vieja = matriz
        matriz = una_izquierda_sin_suma(matriz)
        rep += 1

    igual_derecha = np.diff(matriz) == 0

    for col in range(1,igual_derecha.shape[1]):
        igual_derecha[:,col] = ~(igual_derecha[:,col] * igual_derecha[:,col-1])

    igual_derecha = np.hstack((igual_derecha,[[False]]*len(matriz)))

    lugar = np.where(igual_derecha)
    matriz[lugar]  = matriz[lugar] + matriz[lugar[0] , lugar[1] + 1]
    matriz[lugar[0] , lugar[1] + 1] = 0

    while np.sum(matriz == matriz_vieja) == np.sum(matriz.shape) and rep != len(matriz) - 1:
        matriz_vieja = matriz
        matriz = una_izquierda_sin_suma(matriz)
        rep += 1

    return matriz
##
def random(matriz):
    dos_cuatro = int(1.25*np.random.rand()+1)*2

    ceros = np.where(matriz == 0)
    posicion = int(np.random.rand()*len(ceros[0]))
    matriz[ceros[0][posicion],ceros[1][posicion]] = dos_cuatro
    return matriz

def jugada(matriz,mov):
    if mov == 'a':
        matriz = izquierda(matriz)
        matriz = random(matriz)
    elif mov == 'd':
        matriz = np.flip(matriz)
        matriz = izquierda(matriz)
        matriz = np.flip(matriz)
        matriz = random(matriz)
    elif mov == 'w':
        matriz = np.rot90(matriz)
        matriz = izquierda(matriz)
        matriz = np.rot90(matriz,-1)
        matriz = random(matriz)
    elif mov == 's'
        matriz = np.rot90(matriz,-1)
        matriz = izquierda(matriz)
        matriz = np.rot90(matriz)
        matriz = random(matriz)
    else:
        print('ERROR')
    return matriz
